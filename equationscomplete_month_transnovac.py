#############Packages###########
import pymc

import scipy.stats as st
import pickle
import sys
import operator
#import pykov 
import random
import scipy.integrate as spi
import numpy as np
import pylab as pl
import math as mt
import scipy.linalg as linear
from scipy.linalg import leslie
import decimal
from collections import defaultdict
from matplotlib import pyplot as plt
from matplotlib.pyplot import *
from xlrd import *

from operator import truediv
###########################################
######## Definitions##############
def is_empty(any_structure):
    if any_structure:
        return False
    else:
        return True
					
					


				
###################################################################
class NestedDict(dict):
   def __getitem__(self, key):
        if key in self: return self.get(key)
        return self.setdefault(key, NestedDict())
def extract(DictIn, Dictout):
	for key, value in DictIn.iteritems():
		if isinstance(value, dict): # If value itself is dictionary
			extract(value, Dictout)
		elif isinstance(value, list): # If value itself is list
			for i in value:
				extract(i, Dictout)
		else:
			Dictout[key] = value   

#############################################################
def carriage(a,t,scale):
	carr=0
	expo=0.455
	rateuni=0.885
	rateexp=0.645
# 	if scale=='Year':
# 		t1=t-a*5
# 	elif scale=='Month':
# 		t1=float(t-12*int(t/12))/12
# 	elif scale=='Week':
# 		t1=float(t-52*int(t/52))/52
# 	else:
# 		t1=float(t-365*int(t/365))/365
	if a==0:	
		carr+=0.1*rateuni+0.05*(float(mt.exp(-rateexp*pow(0.5,expo)))+float(mt.exp(-rateexp*pow(1.0,expo))))
		for m in range(1,10):
			carr+=0.1*(float(mt.exp(-rateexp*pow((m),expo))))
	else:
		
			carr+=(float(mt.exp(-rateexp*pow(a*5,expo))))
	if carr<0:
		carr=0		
	return float(carr)
##################################################

def PK(eq,initialcondition,age,sero,infec,startyear,endyear,scale,fa,fc,popage,infant,forceaverage,latency,mu0,alpha,T,b,trans,gammaA,gammaC,trans_decomp,trans_toHCC,trans_DecompCD,trans_HCCHBVD,trans_F,trans_FD):
	Total=np.zeros(endyear-startyear-1)
	children=[]
	infantmod=[]
	m=17####number of groups
	nu=np.array([1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])###birth rate
	
	#lambdaI=np.array([forceaverage[0],forceaverage[1],forceaverage[2],forceaverage[3],forceaverage[4],forceaverage[5],forceaverage[6],forceaverage[7],forceaverage[8],forceaverage[9],forceaverage[10],forceaverage[11],forceaverage[12],forceaverage[13],forceaverage[14],forceaverage[15],forceaverage[16]])
	p=np.array([carriage(0,0,scale),carriage(1,0,scale),carriage(2,0,scale),carriage(3,0,scale),carriage(4,0,scale),carriage(5,0,scale),carriage(6,0,scale),carriage(7,0,scale),carriage(8,0,scale),carriage(9,0,scale),carriage(10,0,scale),carriage(11,0,scale),carriage(12,0,scale),carriage(13,0,scale),carriage(14,0,scale),carriage(15,0,scale),carriage(16,0,scale)])
	#fa=0.85#0.711#fa#float(lat)#0.80#preso da edmunds 19960.7111  # by acute mother # becoming carrier by perinatal infection
	#fc=0.109

	n=[]
	for l in range(0,m):
		n.append(popage[startyear][l])
	n=np.array(n)
	S0=[]
	E0=[]
	I0=[]
	C0=[]
	C1=[]
	C2=[]
	C3=[]
	C4=[]
	C5=[]
	C6=[]
	C7=[]
	Cirr=[]
	HCC=[]
	Decomp=[]
	R0=[]
	HBVD=[]
	Pop02=[]
	if eq==0:
		for l in range(0,m):
			S0.append(initialcondition[l][0]*n[l])
			E0.append(initialcondition[l][1]*n[l])
			I0.append(initialcondition[l][2]*n[l])
			C0.append(initialcondition[l][3]*n[l])
			C1.append(0)
			C2.append(0)
			C3.append(0)
			C4.append(0)
			C5.append(0)
			C6.append(0)
			C7.append(0)
			Cirr.append(0)
			HCC.append(0)
			Decomp.append(0)
			R0.append(initialcondition[l][4]*n[l])
			HBVD.append(0)
			Pop02.append(n[l])
	else:
		for l in range(0,m):
			S0.append(initialcondition[l][0]*n[l])
			E0.append(initialcondition[l][1]*n[l])
			I0.append(initialcondition[l][2]*n[l])
			C0.append(initialcondition[l][3]*n[l])
			C1.append(initialcondition[l][4]*n[l])
			C2.append(initialcondition[l][5]*n[l])
			C3.append(initialcondition[l][6]*n[l])
			C4.append(initialcondition[l][7]*n[l])
			C5.append(initialcondition[l][8]*n[l])
			C6.append(initialcondition[l][9]*n[l])
			C7.append(initialcondition[l][10]*n[l])
			Cirr.append(initialcondition[l][11]*n[l])
			HCC.append(initialcondition[l][12]*n[l])
			Decomp.append(initialcondition[l][13]*n[l])
			R0.append(initialcondition[l][14]*n[l])
			HBVD.append(0)
			Pop02.append(n[l])
		
			
	S0=np.array(S0)#####suscpetible
	E0=np.array(E0)
	I0=np.array(I0)
	C0= np.array(C0)
	C1= np.array(C1)
	C2= np.array(C2)
	C3= np.array(C3)
	C4= np.array(C4)
	C5= np.array(C5)
	C6= np.array(C6)
	C7= np.array(C7)
	Cirr= np.array(Cirr)
	HCC= np.array(HCC)
	Decomp= np.array(Decomp)
	R0= np.array(R0)
	Pop02= np.array(Pop02)
	HBVD=np.array(HBVD)
	ND=MaxTime=139*12
	

	
	year=startyear
	TS=1.0
	

	INPUTVAC=np.hstack((S0,E0,I0,C0,C1,C2,C3,C4,C5,C6,C7,Cirr,HCC,Decomp,R0,Pop02,HBVD))
	p=np.array([carriage(0,0,scale),carriage(1,0,scale),carriage(2,0,scale),carriage(3,0,scale),carriage(4,0,scale),carriage(5,0,scale),carriage(6,0,scale),carriage(7,0,scale),carriage(8,0,scale),carriage(9,0,scale),carriage(10,0,scale),carriage(11,0,scale),carriage(12,0,scale),carriage(13,0,scale),carriage(14,0,scale),carriage(15,0,scale),carriage(16,0,scale)])
	fert=np.array([0,0,0,b[3][0],b[4][0],b[5][0],b[6][0],b[7][0],b[8][0],b[9][0],b[10][0],b[11][0],b[12][0],b[13][0],b[14][0],b[15][0],b[16][0]])#([0,0,0,0,0,0,0,0,0])#b[1][0],b[2][0],b[3][0],b[4][0],b[5][0],b[6][0],b[7][0],b[8][0]])

		
	def diff_eqs(INP,t):  
		'''The main set of equations'''
		Y=np.zeros(17*17)
		V = INP
		k=int(t)
		epsilonup=1.0/12*np.array([0,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2])	
		epsilond=1.0/12*np.array([0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0])
		Pop=[]
		Car=[]
		Ac=[]
		for i in range(0,m):
			Pop.append(V[15*m+i])
			Ac.append(V[2*m+i])
			Car.append(V[3*m+i]+V[4*m+i]+V[5*m+i]+V[6*m+i]+V[7*m+i]+V[8*m+i]+V[9*m+i]+V[10*m+i])
		Pop=np.array(Pop)
		year=startyear+int(k)/12
		
		mu=np.array([mu0[year][0],mu0[year][1],mu0[year][2],mu0[year][3],mu0[year][4],mu0[year][5],mu0[year][6],mu0[year][7],mu0[year][8],mu0[year][9],mu0[year][10],mu0[year][11],mu0[year][12],mu0[year][13],mu0[year][14],mu0[year][15],mu0[year][16]])####moratlity rate

		if k<len(b[1]):
			fert=np.array([0,0,0,b[3][k],b[4][k],b[5][k],b[6][k],b[7][k],b[8][k],b[9][k],b[10][k],b[11][k],b[12][k],b[13][k],b[14][k],b[15][k],b[16][k]])#([0,0,0,0,0,0,0,0,0])#b[1][k],b[2][k],b[3][k],b[4][k],b[5][k],b[6][k],b[7][k],b[8][k]])
		else:
			fert=np.array([0,0,0,b[3][-1],b[4][-1],b[5][-1],b[6][-1],b[7][-1],b[8][-1],b[9][-1],b[10][-1],b[11][-1],b[12][-1],b[13][-1],b[14][-1],b[15][-1],b[16][-1]])



		
		child=0
		childA=0
		childS=0
		childA0=0
		childS0=0
		childB=0 #infant with just birth dose who continues
		childO=0 #infant with just 3 doses 
		childV=0
		
		for i in range(0,m):

			child+=fert[i]*Pop[i]

			childA0+=fert[i]*(fa*Ac[i]+fc*Car[i])

			childS0+=fert[i]*(Pop[i]-(fa*Ac[i]+fc*Car[i]))

		if child >0:
			
			childA=infant[year+1]/child*childA0*1.0/12
			childS=infant[year+1]/child*childS0*1.0/12
					#mort=np.array([0.00037125846825,0.00010538894875,0.0002457375915,0.000503574828,0.00135061587525,0.00305347856725,0.0080537843695,0.027210962198,0.179291737335])
		for i in range(0,m):
			Inf=0
			TotalN=0

			Inf=forceaverage[i]*V[i]
			
						
					
				
			
			mi=-mt.expm1(-mu[i])#1-mt.exp(-mu[i])#mortality
			  
			if i<1:
				Y[i] =childS  -Inf -mi * V[i]-epsilond[i]*V[i]###Susceptible

				Y[(m+i)] =childA+Inf - mi * V[(m+i)] - latency * V[(m+i)]-epsilond[i]*V[m+i]####Latent

				Y[(2*m+i)] =  latency * V[(m+i)] - gammaA * V[(2*m+i)] - mi * V[(2*m+i)]-epsilond[i]*V[2*m+i]#####Acute
				Y[(3*m+i)] = gammaA *p[i]* V[(2*m+i)] - mi * V[(3*m+i)]-gammaC * V[(3*m+i)]-trans*V[(3*m+i)]-epsilond[i]*V[3*m+i]#####Carrier

				Y[(4*m+i)] = trans*V[(3*m+i)]- mi * V[(4*m+i)]-gammaC * V[(4*m+i)]-trans*V[(4*m+i)]-epsilond[i]*V[4*m+i]##C1
				Y[(5*m+i)] = trans*V[(4*m+i)]- mi * V[(5*m+i)]-gammaC * V[(5*m+i)]-trans*V[(5*m+i)]-epsilond[i]*V[5*m+i]##C2
				Y[(6*m+i)] = trans*V[(5*m+i)]- mi * V[(6*m+i)]-gammaC * V[(6*m+i)]-trans*V[(6*m+i)]-epsilond[i]*V[6*m+i]##C3
				Y[(7*m+i)] = trans*V[(6*m+i)]- mi * V[(7*m+i)]-gammaC * V[(7*m+i)]-trans*V[(7*m+i)]-epsilond[i]*V[7*m+i]##C4
				Y[(8*m+i)] = trans*V[(7*m+i)]- mi * V[(8*m+i)]-gammaC * V[(8*m+i)]-trans*V[(8*m+i)]-epsilond[i]*V[8*m+i]##C5
				Y[(9*m+i)] = trans*V[(8*m+i)]- mi * V[(9*m+i)]-gammaC * V[(9*m+i)]-trans*V[(9*m+i)]-epsilond[i]*V[9*m+i]##C6
				Y[(10*m+i)] = trans*V[(9*m+i)]- mi * V[(10*m+i)]-gammaC * V[(10*m+i)]-trans*V[(10*m+i)]-epsilond[i]*V[10*m+i]##C7
				Y[(11*m+i)] = trans*V[(10*m+i)]- mi * V[(11*m+i)]-trans_decomp*V[(11*m+i)]-trans_toHCC*V[(11*m+i)]-epsilond[i]*V[11*m+i]###Cirrhosis
				Y[(12*m+i)] = trans_toHCC*V[(11*m+i)]- mi * V[(12*m+i)]-trans_HCCHBVD*V[(12*m+i)]-epsilond[i]*V[12*m+i]###HCC
				Y[(13*m+i)] = trans_decomp*V[(11*m+i)]- mi * V[(13*m+i)]-trans_DecompCD*V[(13*m+i)]-epsilond[i]*V[13*m+i]###Decompensated

				Y[(14*m+i)] = gammaA *(1.0-p[i])* V[(2*m+i)]+gammaC * (V[(3*m+i)]+V[(4*m+i)]+V[(5*m+i)]+V[(6*m+i)]+V[(7*m+i)]+V[(8*m+i)]+V[(9*m+i)]+V[(10*m+i)]) -mi * V[(14*m+i)]-epsilond[i]*V[14*m+i]#####Recovered
				
				Y[(15*m+i)] = (childS+childA)- mi*(V[(15*m+i)])-trans_HCCHBVD*V[(12*m+i)]-trans_DecompCD*V[(13*m+i)]-epsilond[i]*V[15*m+i] ####population age group
				Y[(16*m+i)] = +trans_HCCHBVD*V[(12*m+i)]+trans_DecompCD*V[(13*m+i)]
			elif i>=1 and i<16:
				Y[i] =  -Inf -mi * V[i]-epsilond[i]*V[i]+epsilonup[i]*V[i-1]###Susceptible
				Y[(m+i)] =+ Inf - mi * V[(m+i)] - latency * V[(m+i)]-epsilond[i]*V[m+i]+epsilonup[i]*V[m+i-1]####Latent
				Y[(2*m+i)] = latency * V[(m+i)] - gammaA * V[(2*m+i)] - mi * V[(2*m+i)]-epsilond[i]*V[2*m+i]+epsilonup[i]*V[2*m+i-1]#####Acute
				Y[(3*m+i)] = gammaA *p[i]* V[(2*m+i)] - mi * V[(3*m+i)]-gammaC * V[(3*m+i)]-trans*V[(3*m+i)]-epsilond[i]*V[3*m+i]+epsilonup[i]*V[3*m+i-1]#####Carrier
				Y[(4*m+i)] = trans*V[(3*m+i)]- mi * V[(4*m+i)]-gammaC * V[(4*m+i)]-trans*V[(4*m+i)]-epsilond[i]*V[4*m+i]+epsilonup[i]*V[4*m+i-1]##C1
				Y[(5*m+i)] = trans*V[(4*m+i)]- mi * V[(5*m+i)]-gammaC * V[(5*m+i)]-trans*V[(5*m+i)]-epsilond[i]*V[5*m+i]+epsilonup[i]*V[5*m+i-1]##C2
				Y[(6*m+i)] = trans*V[(5*m+i)]- mi * V[(6*m+i)]-gammaC * V[(6*m+i)]-trans*V[(6*m+i)]-epsilond[i]*V[6*m+i]+epsilonup[i]*V[6*m+i-1]##C3
				Y[(7*m+i)] = trans*V[(6*m+i)]- mi * V[(7*m+i)]-gammaC * V[(7*m+i)]-trans*V[(7*m+i)]-epsilond[i]*V[7*m+i]+epsilonup[i]*V[7*m+i-1]##C4
				Y[(8*m+i)] = trans*V[(7*m+i)]- mi * V[(8*m+i)]-gammaC * V[(8*m+i)]-trans*V[(8*m+i)]-epsilond[i]*V[8*m+i]+epsilonup[i]*V[8*m+i-1]##C5
				Y[(9*m+i)] = trans*V[(8*m+i)]- mi * V[(9*m+i)]-gammaC * V[(9*m+i)]-trans*V[(9*m+i)]-epsilond[i]*V[9*m+i]+epsilonup[i]*V[9*m+i-1]##C6
				Y[(10*m+i)] = trans*V[(9*m+i)]- mi * V[(10*m+i)]-gammaC * V[(10*m+i)]-trans*V[(10*m+i)]-epsilond[i]*V[10*m+i]+epsilonup[i]*V[10*m+i-1]##C7
				Y[(11*m+i)] = trans*V[(10*m+i)]- mi * V[(11*m+i)]-trans_decomp*V[(11*m+i)]-trans_toHCC*V[(11*m+i)]-epsilond[i]*V[11*m+i]+epsilonup[i]*V[11*m+i-1]###Cirrhosis
				Y[(12*m+i)] = trans_toHCC*V[(11*m+i)]- mi * V[(12*m+i)]-trans_HCCHBVD*V[(12*m+i)]-epsilond[i]*V[12*m+i]+epsilonup[i]*V[12*m+i-1]###HCC
				Y[(13*m+i)] = trans_decomp*V[(11*m+i)]- mi * V[(13*m+i)]-trans_DecompCD*V[(13*m+i)]-epsilond[i]*V[13*m+i]+epsilonup[i]*V[13*m+i-1]###Decompensated
				Y[(14*m+i)] = gammaA *(1.0-p[i])* V[(2*m+i)]+gammaC * (V[(3*m+i)]+V[(4*m+i)]+V[(5*m+i)]+V[(6*m+i)]+V[(7*m+i)]+V[(8*m+i)]+V[(9*m+i)]+V[(10*m+i)]) -mi * V[(14*m+i)]-epsilond[i]*V[14*m+i]+epsilonup[i]*V[14*m+i-1]#####Recovered

		
				Y[(15*m+i)] = - mi*(V[(15*m+i)])-trans_HCCHBVD*V[(12*m+i)]-trans_DecompCD*V[(13*m+i)]-epsilond[i]*V[15*m+i]+epsilonup[i]*V[15*m+i-1] ####population age group
				Y[(16*m+i)] = +trans_HCCHBVD*V[(12*m+i)]+trans_DecompCD*V[(13*m+i)]
			else:
   

				Y[i] =  -Inf -mi * V[i]+epsilonup[i]*V[i-1]###Susceptible
				Y[(m+i)] =+ Inf - mi * V[(m+i)] - latency * V[(m+i)]+epsilonup[i]*V[m+i-1]####Latent
				Y[(2*m+i)] = latency * V[(m+i)] - gammaA * V[(2*m+i)] - mi * V[(2*m+i)]+epsilonup[i]*V[2*m+i-1]#####Acute
				Y[(3*m+i)] = gammaA *p[i]* V[(2*m+i)] - mi * V[(3*m+i)]-gammaC * V[(3*m+i)]-trans*V[(3*m+i)]+epsilonup[i]*V[3*m+i-1]#####Carrier
				Y[(4*m+i)] = trans*V[(3*m+i)]- mi * V[(4*m+i)]-gammaC * V[(4*m+i)]-trans*V[(4*m+i)]+epsilonup[i]*V[4*m+i-1]##C1
				Y[(5*m+i)] = trans*V[(4*m+i)]- mi * V[(5*m+i)]-gammaC * V[(5*m+i)]-trans*V[(5*m+i)]+epsilonup[i]*V[5*m+i-1]##C2
				Y[(6*m+i)] = trans*V[(5*m+i)]- mi * V[(6*m+i)]-gammaC * V[(6*m+i)]-trans*V[(6*m+i)]+epsilonup[i]*V[6*m+i-1]##C3
				Y[(7*m+i)] = trans*V[(6*m+i)]- mi * V[(7*m+i)]-gammaC * V[(7*m+i)]-trans*V[(7*m+i)]+epsilonup[i]*V[7*m+i-1]##C4
				Y[(8*m+i)] = trans*V[(7*m+i)]- mi * V[(8*m+i)]-gammaC * V[(8*m+i)]-trans*V[(8*m+i)]+epsilonup[i]*V[8*m+i-1]##C5
				Y[(9*m+i)] = trans*V[(8*m+i)]- mi * V[(9*m+i)]-gammaC * V[(9*m+i)]-trans*V[(9*m+i)]+epsilonup[i]*V[9*m+i-1]##C6
				Y[(10*m+i)] = trans*V[(9*m+i)]- mi * V[(10*m+i)]-gammaC * V[(10*m+i)]-trans*V[(10*m+i)]+epsilonup[i]*V[10*m+i-1]##C7
				Y[(11*m+i)] = trans*V[(10*m+i)]- mi * V[(11*m+i)]-trans_decomp*V[(11*m+i)]-trans_toHCC*V[(11*m+i)]+epsilonup[i]*V[11*m+i-1]###Cirrhosis
				Y[(12*m+i)] = trans_toHCC*V[(11*m+i)]- mi * V[(12*m+i)]-trans_HCCHBVD*V[(12*m+i)]+epsilonup[i]*V[12*m+i-1]###HCC
				Y[(13*m+i)] = trans_decomp*V[(11*m+i)]- mi * V[(13*m+i)]-trans_DecompCD*V[(13*m+i)]+epsilonup[i]*V[13*m+i-1]###Decompensated

				Y[(14*m+i)] = gammaA *(1.0-p[i])* V[(2*m+i)]+gammaC * (V[(3*m+i)]+V[(4*m+i)]+V[(5*m+i)]+V[(6*m+i)]+V[(7*m+i)]+V[(8*m+i)]+V[(9*m+i)]+V[(10*m+i)]) -mi * V[(14*m+i)]+epsilonup[i]*V[14*m+i-1]#####Recovered

				
				Y[(15*m+i)] = - mi*(V[(15*m+i)])-trans_HCCHBVD*V[(12*m+i)]-trans_DecompCD*V[(13*m+i)]+epsilonup[i]*V[15*m+i-1] ####population age group
				Y[(16*m+i)] = +trans_HCCHBVD*V[(12*m+i)]+trans_DecompCD*V[(13*m+i)]
		return Y   # For odeint
	t_start = 0.0; t_end = ND-1; t_inc = TS
	t_range = np.arange(t_start, t_end+t_inc, t_inc)
	RES = spi.odeint(diff_eqs,INPUTVAC,t_range)
	
#################################################################	
	Susceptible=defaultdict(list)
	Latent=defaultdict(list)
	AcuteInfected=defaultdict(list)
	C0=defaultdict(list)
	C1=defaultdict(list)
	C2=defaultdict(list)
	C3=defaultdict(list)
	C4=defaultdict(list)
	C5=defaultdict(list)
	C6=defaultdict(list)
	C7=defaultdict(list)
	Cirr=defaultdict(list)
	HCC=defaultdict(list)
	HBVD=defaultdict(list)
	Decomp=defaultdict(list)
	Recovered=defaultdict(list)
	Alive=defaultdict(list)
	for a in range(0,m):
		for t in range(0,139*12,12):
			Susceptible[a].append(RES[t,0+a])
			Latent[a].append(RES[t,1*m+a])
			AcuteInfected[a].append(RES[t,2*m+a])
			C0[a].append(RES[t,3*m+a])
			C1[a].append(RES[t,4*m+a])
			C2[a].append(RES[t,5*m+a])
			C3[a].append(RES[t,6*m+a])
			C4[a].append(RES[t,7*m+a])
			C5[a].append(RES[t,8*m+a])
			C6[a].append(RES[t,9*m+a])
			C7[a].append(RES[t,10*m+a])
			Cirr[a].append(RES[t,11*m+a])
			HCC[a].append(RES[t,12*m+a])
			Decomp[a].append(RES[t,13*m+a])
			Recovered[a].append(RES[t,14*m+a])
			Alive[a].append(RES[t,15*m+a])
			if t==0:
				HBVD[a].append(RES[t,16*m+a])
			else:
				HBVD[a].append(RES[t,16*m+a]-RES[t-12,16*m+a])
	# for a in range(0,9):
# 		for t in range(0,141):	
# 			Alive[a].append(Susceptible[a][t]+Latent[a][t]+AcuteInfected[a][t]+C0[a][t]+Recovered[a][t])

	for t in range(0,139*12,12):
		child=sum(fert[i]*RES[t,5*m+i] for i in range(0,m))
		childA=sum(fert[i]*infant[startyear+int(t)/12]/child*(fa*RES[t,2*m+i]+fc*RES[t,3*m+i]) for i in range(0,m))
		childS=sum(fert[i]*infant[startyear+int(t)/12]/child*(RES[t,5*m+i]-(fa*RES[t,2*m+i]+fc*RES[t,3*m+i])) for i in range(0,m))
		infantmod.append(childS+childA)
		children.append(infant[startyear+int(t)/12])	
#################################################################################################
	for a in sorted(Alive.keys()):
			if a !='__name__':
				for t in range(0,len(Alive[a]),1):
					if t !='__name__' and t<=T:
					
						Total[int(t)/1]+=Alive[a][t]
	T=endyear-startyear-1
	susc=np.zeros(T)
	carriers2=np.zeros(T)
	acute=np.zeros(T)
	hcc=np.zeros(T)
	susckeys=sorted(Susceptible.keys())
	for s in susckeys:
		if s !='__name__':
			for t in range(0,len(Susceptible[s]),1):
				if t<T:
					if t !='__name__': 
						
						susc[t]+=float(Susceptible[s][t])/sum(Alive[s1][t] for s1 in range(0,m))
						carriers2[t]+=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])/sum(Alive[s1][t] for s1 in range(0,m))
						acute[t]+=float(AcuteInfected[s][t])/sum(Alive[s1][t] for s1 in range(0,m))#
						hcc[t]+=HCC[s][t]
	rec=np.zeros(T)

	reckeys=sorted(Recovered.keys())
	for s in reckeys:
		if s !='__name__':
			for t in range(0,len(Recovered[s]),1):
				if t<T:
					if t !='__name__': 
						rec[t]+=float(Recovered[s][t])/sum(Alive[s1][t] for s1 in range(0,m))
	Serop1970=[]
	Serop1980=[]
	Serop1990=[]
	Serop2000=[]
	Serop2020=[]
	Serop2040=[]
	Serop2060=[]
	Infec1970=[]
	Infec1980=[]
	Infec1990=[]
	Infec2000=[]
	Infec2020=[]
	Infec2040=[]
	Infec2060=[]
	for s in reckeys:
		if s !='__name__':
			t=20
			carriers=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])
			Serop1970.append(100*float(AcuteInfected[s][t]+Recovered[s][t]+carriers)/Alive[s][t])
			Infec1970.append(100*float(AcuteInfected[s][t]+carriers)/Alive[s][t])
			t=30
			carriers=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])
			Serop1980.append(100*float(AcuteInfected[s][t]+Recovered[s][t]+carriers)/Alive[s][t])
			Infec1980.append(100*float(AcuteInfected[s][t]+carriers)/Alive[s][t])
			t=40
			carriers=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])
			Serop1990.append(100*float(AcuteInfected[s][t]+Recovered[s][t]+carriers)/Alive[s][t])
			Infec1990.append(100*float(AcuteInfected[s][t]+carriers)/Alive[s][t])
			t=50
			carriers=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])
			Serop2000.append(100*float(AcuteInfected[s][t]+Recovered[s][t]+carriers)/Alive[s][t])	
			Infec2000.append(100*float(AcuteInfected[s][t]+carriers)/Alive[s][t])
			t=70
			carriers=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])
			Serop2020.append(100*float(AcuteInfected[s][t]+Recovered[s][t]+carriers)/Alive[s][t])
			Infec2020.append(100*float(AcuteInfected[s][t]+carriers)/Alive[s][t])
			t=90
			carriers=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])
			Serop2040.append(100*float(AcuteInfected[s][t]+Recovered[s][t]+carriers)/Alive[s][t])
			Infec2040.append(100*float(AcuteInfected[s][t]+carriers)/Alive[s][t])
			t=110
			carriers=float(C0[s][t]+C1[s][t]+C2[s][t]+C3[s][t]+C4[s][t]+C5[s][t]+C6[s][t]+C7[s][t])
			Serop2060.append(100*float(AcuteInfected[s][t]+Recovered[s][t]+carriers)/Alive[s][t])								
			Infec2060.append(100*float(AcuteInfected[s][t]+carriers)/Alive[s][t])
	

	return np.array(carriers2),np.array(susc),np.array(Total),np.array(rec),np.array(Serop1970),np.array(Serop1980),np.array(Serop1990),np.array(Serop2000),np.array(Serop2020),np.array(Serop2040),np.array(Serop2060),np.array(acute),np.array(Infec1970),np.array(Infec1980),np.array(Infec1990),np.array(Infec2000),np.array(Infec2020),np.array(Infec2040),np.array(Infec2060),Susceptible,Latent,AcuteInfected,Recovered,C0,C1,C2,C3,C4,C5,C6,C7,Cirr,HCC,Decomp,Alive,np.array(infantmod),np.array(children),hcc,HBVD
	
	
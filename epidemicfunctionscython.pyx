#!/usr/bin/env python
import sys
import pykov
from epidemio_parameters import *
from libcpp.vector cimport vector 
from libcpp.map cimport map 
from libc.stdlib cimport srand,rand,RAND_MAX
from collections import defaultdict

 
#import some often used math functions from the c math library
cdef extern from "math.h":
    double sin(double)
 
cdef extern from "math.h":
    double fabs(double)
 
cdef extern from "math.h":
    double exp(double)
cdef extern from "stdlib.h":
    double drand48()
    void srand48(long int seedval)

cdef extern from "time.h":
    long int time(int)
    
######## Definitions###################################################
cdef float survival
cdef float trans_CAH
cdef int key
cdef int year
 
cdef float InfectionIncidence 
cdef float CarrierIncidence 
cdef float ImmuneIncidence 
cdef float SymptomaticIncidence 
cdef float trans_fdeath 
cdef float trans_fulmi 
cdef float trans_carrierimmu 
cdef float trans_CAH1 
cdef float trans_CAH2 
cdef float trans_CAH3 
cdef float trans_CAH4 
cdef float trans_CAH5 
cdef float trans_CAH6 
cdef float trans_CAH7 
cdef float trans_CAH8 
cdef float extra_death 
cdef float trans_decomp 
cdef float trans_HCCcarrier 
cdef float trans_toHCC 
cdef float trans_Decompcarrier 
cdef float trans_DecompHBVD 
cdef float trans_HCCHBVD


class Age_cohort:
 def __init__(self,float survival,float InfectionIncidence,float CarrierIncidence,float ImmuneIncidence,float SymptomaticIncidence,float trans_fdeath,float trans_fulmi,float trans_carrierimmu,float trans_CAH1,float trans_CAH2,float trans_CAH3,float trans_CAH4,float trans_CAH5,float trans_CAH6,float trans_CAH7,float trans_CAH8,float extra_death,float trans_decomp,float trans_HCCcarrier,float trans_toHCC,float trans_Decompcarrier,float trans_DecompHBVD,float trans_HCCHBVD):
  self.mortality=1-float(survival)
  self.InfectionIncidence=InfectionIncidence
  self.CarrierIncidence=CarrierIncidence
  self.ImmuneIncidence=ImmuneIncidence
  self.SymptomaticIncidence=SymptomaticIncidence
  self.trans_fdeath=trans_fdeath
  self.trans_fulmi=trans_fulmi

  self.trans_carrierimmu=trans_carrierimmu
  self.extra_death=extra_death
  self.trans_CAH1=trans_CAH1
  self.trans_CAH2=trans_CAH2
  self.trans_CAH3=trans_CAH3
  self.trans_CAH4=trans_CAH4
  self.trans_CAH5=trans_CAH5
  self.trans_CAH6=trans_CAH6
  self.trans_CAH7=trans_CAH7
  self.trans_CAH8=trans_CAH8

  self.trans_decomp=trans_decomp
  self.trans_HCCcarrier=trans_HCCcarrier
  self.trans_toHCC=trans_toHCC
  self.trans_Decompcarrier=trans_Decompcarrier
  self.trans_DecompHBVD=trans_DecompHBVD
  self.trans_HCCHBVD=trans_HCCHBVD
 def transition(self):	
  Pchain=pykov.Chain({('Susceptible','Susceptible'):1-self.mortality-self.CarrierIncidence,


  ('Carrier','Susceptible'):self.CarrierIncidence,
  ('Death','Susceptible'):self.mortality,
  ('Vaccinated','Vaccinated'):1.0-self.mortality,
  ('Death','Vaccinated'):self.mortality,




  ('Carrier','Carrier'):1-self.trans_carrierimmu-self.trans_CAH1-self.mortality,
  ('Immune','Carrier'):self.trans_carrierimmu,
  ('CAH1','Carrier'):self.trans_CAH1,
  ('Death','Carrier'):self.mortality,
  ('Immune','Immune'):1-self.mortality,
  ('Death','Immune'):self.mortality,
  ('CAH1','CAH1'):1-self.mortality-self.trans_CAH2-self.trans_carrierimmu,
  ('Immune','CAH1'):self.trans_carrierimmu,
  ('CAH2','CAH1'):self.trans_CAH2,
  ('Death','CAH1'):self.mortality,
  ('CAH2','CAH2'):1-self.mortality-self.trans_CAH3-self.trans_carrierimmu,
  ('Immune','CAH2'):self.trans_carrierimmu,
  ('CAH3','CAH2'):self.trans_CAH3,
  ('Death','CAH2'):self.mortality,
  ('CAH3','CAH3'):1-self.mortality-self.trans_CAH4-self.trans_carrierimmu,
  ('Immune','CAH3'):self.trans_carrierimmu,
  ('CAH4','CAH3'):self.trans_CAH4,
  ('Death','CAH3'):self.mortality,
  ('CAH4','CAH4'):1-self.mortality-self.trans_CAH5-self.trans_carrierimmu,
  ('Immune','CAH4'):self.trans_carrierimmu,
  ('CAH5','CAH4'):self.trans_CAH5,
  ('Death','CAH4'):self.mortality,
  ('CAH5','CAH5'):1-self.mortality-self.trans_CAH6-self.trans_carrierimmu,
  ('Immune','CAH5'):self.trans_carrierimmu,
  ('CAH6','CAH5'):self.trans_CAH6,
  ('Death','CAH5'):self.mortality,
  ('CAH6','CAH6'):1-self.mortality-self.trans_CAH7-self.trans_carrierimmu,
  ('Immune','CAH6'):self.trans_carrierimmu,
  ('CAH7','CAH6'):self.trans_CAH7,
  ('Death','CAH6'):self.mortality,
  ('CAH7','CAH7'):1-self.mortality-self.trans_CAH8-self.trans_carrierimmu,
  ('Immune','CAH7'):self.trans_carrierimmu,		
  ('Cirrhosis','CAH7'):self.trans_CAH8,
  ('Death','CAH7'):self.mortality,
  ('Cirrhosis','Cirrhosis'):1-self.mortality-self.trans_decomp-self.trans_toHCC,
  ('Decomp','Cirrhosis'):self.trans_decomp,
  ('HCC','Cirrhosis'):self.trans_toHCC,
  ('Death','Cirrhosis'):self.mortality,
  ('Decomp','Decomp'):1-self.mortality-self.trans_DecompHBVD-self.trans_toHCC,

  ('HCC','Decomp'):self.trans_toHCC,
  ('HBVDeath','Decomp'):self.trans_DecompHBVD, 
  ('Death','Decomp'):self.mortality,
  ('HCC','HCC'):1-self.trans_HCCHBVD-self.mortality,

  ('HBVDeath','HCC'):self.trans_HCCHBVD,
  ('Death','HCC'):self.mortality,
  ('Death','Death'):1,

  ('HBVDeath','HBVDeath'):1,
  })
		
  return Pchain



cpdef statushcc(incidencedic,prevalence, survival,original_status,trans_CAH,key,year):	
 cdef float trans_CAH1=trans_CAH
 cdef float trans_CAH2=trans_CAH
 cdef float trans_CAH3=trans_CAH
 cdef float trans_CAH4=trans_CAH
 cdef float trans_CAH5=trans_CAH
 cdef float trans_CAH6=trans_CAH
 cdef float trans_CAH7=trans_CAH
 cdef float trans_CAH8=trans_CAH
 cdef float InfectionIncidence
 cdef float CarrierIncidence
 cdef float ImmuneIncidence
 cdef float SymptomaticIncidence	

 if key<100:
  sumalive=0
  status=('Susceptible','Carrier','Immune','CAH1','CAH2','CAH3','CAH4','CAH5','CAH6','CAH7','Cirrhosis','Decomp','HCC','HBVDeath','Death','Vaccinated')
  InfectionIncidence=incidencedic[key][0]

  if original_status['Susceptible']>0:  
   if key==0:
 
    CarrierIncidence=(prevalence[1]-original_status['Carrier'])/original_status['Susceptible'] *(original_status['Susceptible']-original_status['Vaccinated'])/original_status['Susceptible']
    SymptomaticIncidence=(prevalence[1]-original_status['Carrier'])/original_status['Susceptible']   *(original_status['Susceptible']-original_status['Vaccinated'])/original_status['Susceptible']
   else: 
    diff=(prevalence[key+1]*(1-original_status['HBVDeath']-original_status['Death'])-(original_status['Carrier']+original_status['CAH1']+original_status['CAH2']+original_status['CAH3']+original_status['CAH4']+original_status['CAH5']+original_status['CAH6']+original_status['CAH7']))
    if diff>0:  
     CarrierIncidence=diff/original_status['Susceptible']*(original_status['Susceptible']-original_status['Vaccinated'])/original_status['Susceptible']
     SymptomaticIncidence=diff/original_status['Susceptible']*(original_status['Susceptible']-original_status['Vaccinated'])/original_status['Susceptible']
    else:
     CarrierIncidence=0
     SymptomaticIncidence=0
     #SymptomaticIncidence=incidencedic[key][1]/original_status['Susceptible']
  else:
   CarrierIncidence=0.0 
   SymptomaticIncidence=0.0
  ImmuneIncidence=incidencedic[key][2]

 else:
  InfectionIncidence=0
  CarrierIncidence=0
  ImmuneIncidence=0
  SymptomaticIncidence=0
 if key<20:
  agegroup=Age_cohort(survival,InfectionIncidence,CarrierIncidence,ImmuneIncidence,SymptomaticIncidence,trans_fdeath,trans_fulmi,0.0,trans_CAH1,trans_CAH2,trans_CAH3,trans_CAH4,trans_CAH5,trans_CAH6,trans_CAH7,trans_CAH8,extra_death,trans_decomp,trans_HCCcarrier,trans_toHCC,trans_Decompcarrier,trans_DecompHBVD,trans_HCCHBVD)

 else:		
  agegroup=Age_cohort(survival,InfectionIncidence,CarrierIncidence,ImmuneIncidence,SymptomaticIncidence,trans_fdeath,trans_fulmi,0.02,trans_CAH1,trans_CAH2,trans_CAH3,trans_CAH4,trans_CAH5,trans_CAH6,trans_CAH7,trans_CAH8,extra_death,trans_decomp,trans_HCCcarrier,trans_toHCC,trans_Decompcarrier,trans_DecompHBVD,trans_HCCHBVD)
 M=agegroup.transition()

 final_status=pykov.Vector()
 temporiginal=pykov.Vector()
 statuskey=original_status.keys()

 for st in statuskey:
  temporiginal[st]=original_status[st]
 temporiginal.normalize()	
 final_status=M*temporiginal
 return final_status	